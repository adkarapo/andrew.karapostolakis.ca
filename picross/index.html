<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8"/>
		<title>How to Run Mario's Picross</title>
		<link rel="stylesheet" type="text/css" href="index.css">
	</head>
	<body>
		<h1>How to Run <i>Mario's Picross</i></h1>
		<p>
			Nintendo's <i>Picross</i> games were originally designed to run on video game consoles. To run them on a normal
			computer, you need to use a program called an emulator.
		</p>
		<p>
			Here's how to do it:
		</p>

		<h2>Setup</h2>
		<ol>
			<li>
				<p>
					Create a new folder somewhere on your computer to store the games and the emulator. I called mine "games".
				</p>
				<ul>
					<li>
						Note: you don't have to put the files anywhere in particular, this is just how I would do it. The main
						thing is that you know where all the game files are.
					</li>
				</ul>
			</li>
			<li>
				<p>
					Go to <a href="https://github.com/bsnes-emu/bsnes/releases" target="_blank" rel="noopener noreferrer">
					https://github.com/bsnes-emu/bsnes/releases</a>. Find "bsnes_v115-windows.zip" (or whichever is marked
					"Latest") and click to download it. This file contains the emulator which will run the games.
				</p>
				<img src="img/01-bsnes-dl.png"/>
			</li>
			<li>
				<p>Save the file I emailed you to your computer. This one contains the actual games.</p>
			</li>
			<li>
				<p>
					Right-click on each of the files you just downloaded, and select "Extract All...".
				</p>
				<img src="img/02-extract.png"/>
				<p>
					When it asks you to select a destination, click "Browse..." and select the "games" folder you made earlier
					(or whatever you called it). At this point, you should have a folder that looks something like this:
				</p>
				<img src="img/03-extracted.png"/>
			</li>
			<li>
				<p>Open the "bsnes_v115-windows" folder and run "bsnes". This is the emulator program.</p>
			</li>
		</ol>

		<h2>Controls</h2>
		<p>
			Since the games are designed to run on a game console rather than a computer, they expect you to be using a game
			controller (like <a href="https://upload.wikimedia.org/wikipedia/commons/6/69/SNES-Controller-Flat.jpg"
			target="_blank" rel="noopener noreferrer">this</a>). The emulator uses the keyboard to simulate a game controller.
			All the in-game instructions will refer to controller buttons, and the mouse will not work inside the game.
		</p>
		<p>Here are the main buttons used for Picross, and their corresponding keyboard keys.</p>
		<table>
			<tr>
				<th>Controller button</th>	<th>Keyboard key</th>
			</tr>
			<tr>
				<td>Up/Down/Left/Right</td>	<td>Arrow keys</td>
			</tr>
			<tr>
				<td>B</td>					<td>Z</td>
			</tr>
			<tr>
				<td>A</td>					<td>X</td>
			</tr>
			<tr>
				<td>Start</td>				<td>Enter</td>
			</tr>
		</table>
		<p>In game menus, A is "OK" and B is "back" or "cancel". Start is used to pause the game and open the game settings.</p>
		<p id="input">
			If you want to use different keyboard keys, click <a href="input.html">here</a> for instructions on how to change
			them.
		</p>

		<h2>Let's play!</h2>
		<p>
			There are three games included in the "picross" folder:
			<ul>
				<li>Mario's Picross (released March 1995)</li>
				<li>Mario's Super Picross (released September 1995)</li>
				<li>Picross 2 (released October 1996)</li>
			</ul>
		</p>

		<p>
			To start, open bsnes, and select "System", then "Load Game ..." from the menu. A menu will appear asking you to
			select a game.
		</p>
		<p>
			If you want to play <i>Mario's Picross</i> or <i>Picross 2</i>, find the "picross" folder, then choose "Super Game
			Boy" from the list. Another menu will appear. Find the "picross" folder again, then select <i>Mario's Picross</i> or
			<i>Picross 2</i> from the list.
		</p>
		<img src="img/07-sgb-combined.png"/>
		<p>
			If you want to play <i>Mario's Super Picross</i>, find the "picross" folder, then choose <i>Mario's Super
			Picross</i> from the list.
		</p>
		<img src="img/08-sfc.png"/>
		<p id="platform">(If you're curious about why this is necessary, click <a href="platform.html">here</a>.)</p>
		<p>Alternatively, if you've played a game before, you can look for it in the "Load Recent Game" menu.</p>
		<ul>
			<li>
				Note: There's a possibility that your computer might not be fast enough to run this emulator properly. At normal
				speed, the indicator in the bottom right corner of the screen should read "60 FPS", and the title screen music
				for <i>Mario's Picross</i> should sound like <a href="https://www.youtube.com/watch?v=8aVaXoC1PoA"
				target="_blank" rel="noopener noreferrer">this</a>. If the game seems slow, let me know and I can find another
				solution.
			</li>
		</ul>
		<p>
			Now you're ready to play! Press A or Start at the title screen to start the game. The games will save automatically
			after you complete each puzzle.
		</p>

		<h2>Video (Optional section)</h2>
		<p>
			These games were originally designed to be played on CRT televisions, and will look a little different on a modern
			computer monitor. If you'd like to make them look more like they would on a TV, there are a couple of settings you
			can	change:
		</p>
		<ul>
			<li>
				From the menu in bsnes, select "Settings", then "Output", and turn on Aspect Correction. This will stretch the
				screen to the dimensions of a TV set. I would especially recommend this setting for <i>Mario's Super
				Picross</i>.
			</li>
			<li>
				To make the image look like it's coming from an old TV set, there are a few different options available:
				<ul>
					<li>
						In Settings &gt; Shader, select "None". In Settings &gt; Filter, select one of the "NTSC" options. The
						different options simulate different TV connections.
					</li>
					<li>
						In Settings &gt; Shader, select "NTSC" to simulate a North American TV, or "PAL-Composite" for a
						European TV. In Settings &gt; Filter, select "None", or one of the "Scanlines" options.
					</li>
				</ul>
			</li>
		</ul>
		<p>These are the options I would suggest, but you can try out the rest and see which ones you like.</p>
		<p>You can also toggle fullscreen mode using the F11 key.</p>

		<h2>Notes on releases</h2>
		<p>
			<i>Mario's Super Picross</i> and <i>Picross 2</i> were not officially released outside of Japan, and contain several
			Japanese cultural references in addition to the Mario references in the first game. The versions in the "picross"
			folder are fan translations - the original Japanese versions are in the "jp" folder and can be played using the same
			procedure as their respective English versions.
		</p>
	</body>
</html>
